package com.finalexam.trujillo.app.reniec.service;

import com.finalexam.trujillo.app.reniec.model.response.ReniecResponse;
import io.reactivex.Single;

public interface IReniecService {

    Single<ReniecResponse> validateReniec(String document);


}
