package com.finalexam.trujillo.app.reniec.service;

import com.finalexam.trujillo.app.reniec.model.response.ReniecResponse;
import io.reactivex.Single;
import org.springframework.stereotype.Service;

@Service
public class ReniecServiceImpl implements IReniecService {
    @Override
    public Single<ReniecResponse> validateReniec(String document) {
        return Single.just(ReniecResponse.builder()
                        .entityName("Reniec")
                        .success(true)
                        .build());
    }
}
