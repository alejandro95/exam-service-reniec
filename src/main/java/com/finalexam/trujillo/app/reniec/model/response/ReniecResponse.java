package com.finalexam.trujillo.app.reniec.model.response;


import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ReniecResponse {

    private String entityName;
    private Boolean success;

}
