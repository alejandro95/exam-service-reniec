package com.finalexam.trujillo.app.reniec;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExamServiceReniecApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExamServiceReniecApplication.class, args);
    }

}
