package com.finalexam.trujillo.app.reniec.model.entity;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Reniec {

    private String entityName;
    private Boolean success;

}
