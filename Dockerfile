FROM maven:3.6-jdk-8-alpine AS builder
VOLUME /tmp
ADD ./target/exam-service-reniec-0.0.1-SNAPSHOT.jar service-reniec.jar
ENTRYPOINT ["java","-jar","/service-reniec.jar"]