package com.finalexam.trujillo.app.reniec.controller;

import com.finalexam.trujillo.app.reniec.model.request.ReniecRequest;
import com.finalexam.trujillo.app.reniec.model.response.ReniecResponse;
import com.finalexam.trujillo.app.reniec.service.IReniecService;
import io.reactivex.Single;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/external/reniec")
public class ReniecController {


    @Autowired
    private IReniecService iReniecService;
    @PostMapping("/validate")
    public Single<ReniecResponse> validateReniec(@RequestBody ReniecRequest reniecRequest ){

        return iReniecService.validateReniec(reniecRequest.getDocument());
    }

}
